package com.example.kjankiewicz.android_16w02_googlemapapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(),
        OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private var mMap: GoogleMap? = null
    //private var mMapReady = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val options = GoogleMapOptions()
        val centerPos = LatLng(52.406459, 16.941649)
        val camPosition = CameraPosition(centerPos, 15f, 0f, 0f)
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                //.liteMode(true)
                .camera(camPosition)
                .compassEnabled(true)
                .rotateGesturesEnabled(true)
                .scrollGesturesEnabled(true)
                .tiltGesturesEnabled(true)
                .zoomControlsEnabled(true)
                .zoomGesturesEnabled(true)

        val mapFragment = SupportMapFragment.newInstance(options)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.mapContainer, mapFragment)
        fragmentTransaction.commit()
        mapFragment.getMapAsync(this)
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val poznan = LatLng(52.408513, 16.934110)
        val poznanMarker = mMap?.addMarker(MarkerOptions()
                .position(poznan)
                .title("Poznań"))
        poznanMarker?.showInfoWindow()

        val cwPut = LatLng(52.404220, 16.949610)
        val cwPutMarkerOptions = MarkerOptions()
                .position(cwPut)
                .alpha(0.5.toFloat())
                .draggable(true)
                .visible(true)
                .rotation(50f)
                .flat(true)
                .anchor(0.5.toFloat(), 0.5.toFloat())
                .snippet("Centrum Wykładowe")
                .title("Politechnika Poznańska")
        mMap?.addMarker(cwPutMarkerOptions)
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(cwPut, 14f))

        mMap?.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            private val parent = findViewById<ViewGroup>(R.id.mapContainer)
            private val contents = layoutInflater.inflate(R.layout.info_window, parent, false)

            override fun getInfoWindow(marker: Marker): View? {
                return null
            }

            override fun getInfoContents(marker: Marker): View {
                val title = marker.title
                val txtTitle = contents.findViewById<TextView>(R.id.txtInfoWindowTitle)
                if (title != null) {
                    txtTitle.text = title
                    val imageView = contents.findViewById<ImageView>(R.id.ivInfoPicture)
                    if (title == "Poznań") {
                        imageView.setImageResource(R.drawable.ratusz)
                    }
                    if (title == "Politechnika Poznańska") {
                        imageView.setImageResource(R.drawable.pp_cw)
                    }
                } else {
                    txtTitle.text = ""
                }

                val snippet = marker.snippet
                val txtSnippet = contents.findViewById<TextView>(R.id.txtInfoWindowSnippet)
                if (snippet != null) {
                    txtSnippet.text = snippet
                } else {
                    txtSnippet.text = ""
                }
                return contents
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_google_map, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_google_map_normal_type) {
            mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            return true
        }

        if (id == R.id.action_google_map_hybrid_type) {
            mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
            return true
        }

        if (id == R.id.action_google_map_satellite_type) {
            mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
            return true
        }

        if (id == R.id.action_google_map_terrain_type) {
            mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
            return true
        }

        if (id == R.id.action_google_map_none_type) {
            mMap?.mapType = GoogleMap.MAP_TYPE_NONE
            return true
        }

        if (id == R.id.action_google_map_fly_up) {
            mMap?.apply {
                val poznan = LatLng(52.408513, 16.934110)
                moveCamera(CameraUpdateFactory.newLatLngZoom(poznan, 15f))
                animateCamera(CameraUpdateFactory.zoomIn())
                animateCamera(CameraUpdateFactory.zoomTo(10f), 8000, null)
            }
            return true
        }

        if (id == R.id.action_google_map_fly_far) {
            mMap?.apply {
                val poznan = LatLng(52.408513, 16.934110)
                val giewont = LatLng(49.251190, 19.930016)
                moveCamera(CameraUpdateFactory.newLatLngZoom(poznan, 15f))
                animateCamera(CameraUpdateFactory.zoomIn())
                val cameraPosition = CameraPosition.Builder()
                        .target(giewont)
                        .zoom(17f)
                        .bearing(90f)
                        .tilt(30f)
                        .build()
                animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition), 20000, null)
            }
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onInfoWindowClick(marker: Marker) {
        marker.hideInfoWindow()
    }
}
